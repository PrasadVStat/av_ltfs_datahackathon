#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd #For reading the dataset and performing data munging 
import numpy as np # For performing cretain airthmetic operations
import matplotlib.pyplot as plt # For plotting
import seaborn as sns # For plotting different graphs and plots
import time
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import MinMaxScaler
import platform
import os
from sklearn.metrics import roc_auc_score
from skopt import dump, load
import gc



# In[8]:

## Garbage Collection
gc.collect()


## Function for pre-processing the data

def preprocess(Dataset,test_size_split=0.3):
        
    
    
    ### Columns which are to be converted into object column 
    Cols_change_category= ["branch_id","supplier_id","manufacturer_id","Current_pincode_ID"
                       ,"Employment.Type","State_ID","Employee_code_ID","MobileNo_Avl_Flag"
                       ,"Aadhar_flag","PAN_flag","VoterID_flag","Driving_flag","Passport_flag"
                       ,"PERFORM_CNS.SCORE.DESCRIPTION"]
    
    for col in Cols_change_category:
        Dataset[col] = Dataset[col].astype('object',copy=False)
    
    ### Dropping Variables with high number of classes
    variables = list(Dataset.drop(['Current_pincode_ID','Employee_code_ID','supplier_id','branch_id'
                                   ,"PERFORM_CNS.SCORE.DESCRIPTION"],axis=1))
    Dataset= Dataset[variables]
    
    ### Performing One-hot encoding
    Dataset= pd.get_dummies(Dataset, prefix_sep='_', drop_first=True)
    
    ### Splitting Train & Test Variables
    Test= Dataset[Dataset['loan_default']==3]
    Train= Dataset[Dataset['loan_default']!=3]
    
    ### Dropping "UniqueID" (ID variable) and "loan_deafult" (Target Variable)
    variables1 = list(Train.drop(['UniqueID','loan_default'],axis=1))
    X= Train[variables1]
    y= Train["loan_default"]
    X_Test=Test[variables1]
    
    transformer_X_train = RobustScaler().fit(X)
    X1= transformer_X_train.transform(X)
    
    transformer_X_Test = RobustScaler().fit(X_Test)
    X1_Test= transformer_X_Test.transform(X_Test)
    
     
    X_train, X_validation, y_train, y_validation = train_test_split(X1, y, test_size=test_size_split, random_state=11
                                                                    ,shuffle=True,stratify=y)

    return X_train, X_validation, y_train, y_validation,X1_Test
        
gc.collect()

# In[13]:

def preprocess_minmax(Dataset,test_size_split=0.3):
        
    
    
    ### Columns which are to be converted into object column 
    Cols_change_category= ["branch_id","supplier_id","manufacturer_id","Current_pincode_ID"
                       ,"Employment.Type","State_ID","Employee_code_ID","MobileNo_Avl_Flag"
                       ,"Aadhar_flag","PAN_flag","VoterID_flag","Driving_flag","Passport_flag"
                       ,"PERFORM_CNS.SCORE.DESCRIPTION"]
    
    for col in Cols_change_category:
        Dataset[col] = Dataset[col].astype('object',copy=False)
    
    ### Dropping Variables with high number of classes
    variables = list(Dataset.drop(['Current_pincode_ID','Employee_code_ID','supplier_id','branch_id'
                                   ,"PERFORM_CNS.SCORE.DESCRIPTION"],axis=1))
    Dataset= Dataset[variables]
    
    ### Performing One-hot encoding
    Dataset= pd.get_dummies(Dataset, prefix_sep='_', drop_first=True)
    
    ### Splitting Train & Test Variables
    Test= Dataset[Dataset['loan_default']==3]
    Train= Dataset[Dataset['loan_default']!=3]
    
    ### Dropping "UniqueID" (ID variable) and "loan_deafult" (Target Variable)
    variables1 = list(Train.drop(['UniqueID','loan_default'],axis=1))
    X= Train[variables1]
    y= Train["loan_default"]
    X_Test=Test[variables1]
    
    transformer_X_train = MinMaxScaler(feature_range=(0,1)).fit(X)
    X1= transformer_X_train.transform(X)
    
    transformer_X_Test = MinMaxScaler(feature_range=(0,1)).fit(X_Test)
    X1_Test= transformer_X_Test.transform(X_Test)
    
     
    X_train, X_validation, y_train, y_validation = train_test_split(X1, y, test_size=test_size_split, random_state=11
                                                                    ,shuffle=True,stratify=y)
    gc.collect()
    return X_train, X_validation, y_train, y_validation,X1_Test



def Validation_Scoring(clf,X_validation,y_validation,Model_name):
    
 #   from sklearn.model_selection import train_test_split

  #  X_train, X_validation, y_train, y_validation = train_test_split(X, y, test_size=0.3, random_state=11)
    prediction_val = clf.predict_proba(X_validation)[:,1]
    threshold= [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    correct=y_validation
    clf_score= []
    for i in threshold:
        prediction_val_class= np.where(prediction_val>= i,1,0)
        score= roc_auc_score(np.array(correct,dtype=int),prediction_val_class)
        print("The roc_auc_score of "+ Model_name+ "with Threshold",i,"is", score)
        clf_score.append(score)
		


# In[14]:


def Predicting_Test(clf,Test_Data,Original_Test_Data,Output_path,Threshold=0.5):
   
    
    Test_Prediction= clf.predict_proba(Test_Data)[:, 1]
    Threshold = Threshold
    
    Test_Prediction_class= np.where(Test_Prediction>= Threshold,1,0)
    Test_Prediction_class_df= pd.DataFrame(Test_Prediction_class)
    Test_Prediction_class_df.columns=['loan_default']
    DF_Test= pd.concat([Original_Test_Data["UniqueID"],Test_Prediction_class_df],axis=1) #Using original Test data for "UniqueID" 
    
    File_Time=time.strftime("%Y%m%d-%H%M%S")
    Solution_Model=clf.estimator.__class__.__name__+"_"+str(Threshold)
    Output_model=Output_path+Solution_Model+"_"+File_Time+ ".csv"
    gc.collect()
    DF_Test.to_csv(Output_model,index=False)


# In[15]:


def Saving_Model(clf,Model_path,Model_name):
    

    File_Time=time.strftime("%Y%m%d-%H%M%S")
    save_model_path= Model_path + str(Model_name+"_"+File_Time+".pkl")
    dump(clf, save_model_path)


# In[8]:


def Loading_Model(filename,Model_path):
    
    load_model_path= Model_path + filename
    Model_loaded_pkl = load(load_model_path)
    return Model_loaded_pkl


# In[24]:


global start
start = 0


# In[25]:


def report_perf(optimizer, X, y, title):
    
    global start
    start = time.perf_counter()
    print ("Model Optimization started at ", time.strftime("%H:%M:%S"))
    return optimizer.fit(X, y,callback=status_print)
    print(title, "best CV score:", optimizer.best_score_)


# In[26]:


def status_print(optim_result):
    """Status callback durring bayesian hyperparameter search"""
    global start
    #start = time.clock()
    # Get all the models tested so far in DataFrame format
    all_models = pd.DataFrame(opt.cv_results_) 
    print('MODEL #' ,len(all_models))
    print("Best ROC-AUC:", np.round(opt.best_score_, 4))
    #print ("Iteration for Model",str(len(all_models)),"Started at", time.strftime("%H:%M:%S"))
    # Get current parameters and the best parameters    
    best_params = pd.Series(opt.best_params_)
    print('Best params: {}\n'.format(opt.best_params_))
    
    clf_name = opt.estimator.__class__.__name__
    all_models.to_csv(Model_path+clf_name+"_cv_results.csv")
    end = time.perf_counter()
    print("Time taken is",str(end-start)," seconds")
    print()
    start = time.perf_counter()

